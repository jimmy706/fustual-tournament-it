package com.axonactive.footballtournament;
public class BaseTest {
    protected static final String LOCAL_REST_URI = "http://localhost:8080";

    protected String getBaseUri() {
        return LOCAL_REST_URI;
    }

    protected String getBaseApiUri() {
        return LOCAL_REST_URI + "/api";
    }


}
