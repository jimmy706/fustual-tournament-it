package com.axonactive.footballtournament;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MemberResourceTest extends BaseTest {
    private MemberResourceClient client;

    @BeforeEach
    public void createInstances() {
        ResteasyClientBuilder clientBuilder = new ResteasyClientBuilder();
        ResteasyWebTarget target = clientBuilder.build().target(UriBuilder.fromPath(getBaseApiUri()));
        client = target.proxy(MemberResourceClient.class);
    }

    @Test
    void whenInsertValidMemberThenSaveRecoredToDB() {

        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("firstName", "John");
        builder.add("lastName", "Doe");
        builder.add("age", 21);
        builder.add("gender", "MALE");
        builder.add("socialInsuranceId", "AAVN");
        builder.add("playerNumber", "09");

        Response response = client.addMember(builder.build());

        assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    void whenGetAllMembersReceiveListOfMembers() {
        Response response = client.getAllMembers();

        assertEquals(Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    void when_RequestWithMissingFieldsAddMember_ThrowError() {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("lastName", "Doe");
        builder.add("age", 21);
        builder.add("gender", "MALE");
        builder.add("socialInsuranceId", "AAVN");
        builder.add("playerNumber", "09");

        Response response = client.addMember(builder.build());
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    void when_RequestSpecifitMemberById_Receive1Member() {
        Response response = client.getMemberById(1);

        assertEquals(Status.OK.getStatusCode(), response.getStatus());

    }

    @Test
    void when_RequestUnExistedMember_ThrowNotFound() {
        Response response = client.getMemberById(9090);

        assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

}
