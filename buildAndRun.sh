#!/bin/sh
if [ $(docker ps -a -f name=fustaltournament-it | grep -w fustaltournament-it | wc -l) -eq 1 ]; then
  docker rm -f fustaltournament-it
fi
mvn clean package && docker build -t com.axonactive.fustaltournament.it/fustaltournament-it .
docker run -d -p 9080:9080 -p 9443:9443 --name fustaltournament-it com.axonactive.fustaltournament.it/fustaltournament-it
