@echo off
call mvn clean package
call docker build -t com.axonactive.fustaltournament.it/fustaltournament-it .
call docker rm -f fustaltournament-it
call docker run -d -p 9080:9080 -p 9443:9443 --name fustaltournament-it com.axonactive.fustaltournament.it/fustaltournament-it